<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CBT LOGIN</title>
    <link rel="stylesheet" type="text/css" href="bootstrap\dist\css\bootstrap.css">
    <link rel="stylesheet" type="text/css" href="moodlelogin.css">
</head>
<body>
    <!-- HEADER -->
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-9 col-md-9">
                    <table width="100%" border="0">
                        <tbody>
                            <tr>
                                <td rowspan="3" width="90px" align="center">
                                    <a href="#"><img src="images/img.jpg" height="90" style="margin-top: 10px;"></a>
                                </td>
                                <td class="h-atas"><h2><b>CBT LOGIN</b></h1></td>
                            </tr>
                            <tr>
                                <td class="h-atas"><p>SMA NEGERI 6 SURABAYA</p></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-3 col-md-3 right">
                    <table width="100%" border="0" style="margin-top: 10px; margin-bottom: 10px;">
                        <tbody>
                            <tr>
                                <td rowspan="3" width="90px" align="center">
                                    <img src="images/avatar.gif" height="90" style="vertical-align: middle; margin-left: 0px;">
                                </td>
                                <td class="h-atas"><span>Selamat Datang</span></td>
                            </tr>
                            <tr>
                                <td class="h-atas"><span><b>Siswa Peserta Ujian</b></span></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>    
            </div>
        </div>
    </header>
    <!-- HEADER -->

    <ul class="u-separate">
        <div class="alert alert-danger"></div>
    </ul>

    <!-- FORM -->
    <div class="col-md-4 col-md-offset-4 login-wrapper" style="float: inherit;">
        <div class="panel panel-default" id="panel-default">
            <div class="panel-heading" id="panel-heading">User Login</div>
            <div class="inner-content">
                <!-- change the action to your IP/Moodle site **DON'T FORGET** the login/index.php -->
                <form action="http://127.0.0.1/login/index.php" method="POST">
                    <div class="form-horizontal" id="f-login">
                        <div class="form-group">
                            <label for="Username" class="col-sm-3 col-md-3 control-label" >Username</label>
                            <div class="col-sm-8 col-md-8 input-group">
                                <span class="input-group-addon glyphicon glyphicon-user icon"></span>
                                <input class="form-control inp" type="text" name="username" placeholder="username" id="i-user">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Username" class="col-sm-3 col-md-3 control-label" >Password</label>
                            <div class="col-sm-8 col-md-8 input-group">
                                <span class="input-group-addon glyphicon glyphicon-lock icon"  id="i-pass"></span>
                                <input class="form-control inp" type="text" name="password" placeholder="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-8">
                                <button class="btn btn-success btn-block tombol" type="submit">LOGIN</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- FORM -->

    <!-- FOOTER -->
    <footer>
        <div class="container-fluid">
            <p>Copyright SMA NEGERI 6 SURABAYA | Design By proesd</p>
        </div>
    </footer>
    <!-- FOOTER -->
</body>
</html>